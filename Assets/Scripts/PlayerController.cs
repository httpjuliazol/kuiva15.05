﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Публичные конфигурации
    [SerializeField] float moveSpeed = 0f;
    [SerializeField] float JumpSpeed = 0f;
    [SerializeField] float AngularDrag = 0f;
    [SerializeField] AudioClip[] stepssound;
    [SerializeField] AudioClip jumpsound;

    //Состояния
    private bool isDeath = false;
    private bool isKuiva = false;
    private bool isJump = false;
    private float Stepstime = 5;
    private float timeout;


    //создание объекта
    Animator myanimation;
    Rigidbody2D myrigidbody;
    CapsuleCollider2D bodycollider;
    BoxCollider2D feetcollider;
    void Start()
    {
        myanimation = GetComponent<Animator>();
        myrigidbody = GetComponent<Rigidbody2D>();
        bodycollider = GetComponent<CapsuleCollider2D>();
        feetcollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Run();
        if (!isKuiva)
        {
            Jump();
        }
        Flip();
        Kuiva();
    }

    void Run()
    {

        if (Input.GetButton("Fire1"))
        {
            float move = Input.GetAxis("Horizontal");
            myrigidbody.velocity = new Vector2(move / 3 * moveSpeed, myrigidbody.velocity.y);
           // bodycollider.size = new Vector2 (1, bodycollider.size.y);
            //anim.SetFloat("Speed", Mathf.Abs(move));
        }

        else
        {
            //0.1614743
            timeout += Time.deltaTime;
            float move = Input.GetAxis("Horizontal");
            myrigidbody.velocity = new Vector2(move * moveSpeed, myrigidbody.velocity.y);
            myanimation.SetFloat("MoveSpeed", Mathf.Abs(move));
            if (Mathf.Abs(move) > 0 && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")) && timeout > 0.3)
            {
                AudioSource.PlayClipAtPoint(stepssound[Random.Range(0, stepssound.Length)], Camera.main.transform.position);
                timeout = 0;
            }

        }
    }

    /* IEnumerable SoundStep()
     {
         AudioSource.PlayClipAtPoint(stepssound,  Camera.main.transform.position);
         yield return new WaitForSeconds(StepTime);
     }*/

    void Flip()
    {
        if (Mathf.Abs(myrigidbody.velocity.x) > 0)
        {
            transform.localScale = new Vector2(Mathf.Sign(myrigidbody.velocity.x) * 2, 2f);
        }
    }

    void Kuiva()
    {
        if (Input.GetButton("Fire1"))
        {
            myanimation.SetBool("isKuiva", true);
            isKuiva = true;
        }

        else { myanimation.SetBool("isKuiva", false);
            isKuiva = false;
        }

    }

    void Jump()
    {
 
        if (feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            myanimation.SetBool("isJump", false);
            myanimation.SetFloat("JumpSpeed", 0f);
        }

        if (Input.GetButton("Jump") && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        {
            myanimation.SetBool("isJump", true);
            myrigidbody.AddForce(new Vector2(0f, JumpSpeed), ForceMode2D.Impulse);
            AudioSource.PlayClipAtPoint(jumpsound, transform.position);
        }

        if (!feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
        { myanimation.SetFloat("JumpSpeed", myrigidbody.velocity.y); }
    }

    /* private void Death()
    {
        if (feet.IsTouchingLayers(LayerMask.GetMask("Traps")))
        {
            transform.rotation = Quaternion.Euler(0, 0, 90);
            isAlive = !isAlive;
            anim.SetFloat("Speed", 0);
        }
    }

    //Debug.Log(myanimation.GetBool("isJump"));
    /*Debug.Log(myrigidbody.velocity.y);

    if (feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    {
        myanimation.SetBool("isJump", false);
    }

    if (!feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    { return; }

    if (Input.GetButtonDown("Jump") && feetcollider.IsTouchingLayers(LayerMask.GetMask("Ground")))
    {
        if 
        myanimation.SetBool("isJump", true);
        myanimation.SetFloat("JumpSpeed", myrigidbody.velocity.y);

        // myrigidbody.velocity += new Vector2(0f, JumpSpeed);
        myrigidbody.AddForce(new Vector2(0f, JumpSpeed), ForceMode2D.Impulse);

    }*/

}
