﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scrolling : MonoBehaviour
{
    private float length, startposx;
    public GameObject cam;
    public float parallaxEffects;
    public float posy;
    public float predelsee;



    void Start()
    {
        startposx = transform.position.x;
       // length = GetComponent<SpriteRenderer>().bounds.size.x;

    }
       
    // Update is called once per frame
    void FixedUpdate()
    {
        float temp = (cam.transform.position.x * (1 - parallaxEffects));
        float distx = (cam.transform.position.x * parallaxEffects);
    
        transform.position = new Vector3(startposx + distx, Mathf.Clamp(cam.transform.position.y,2,predelsee) - posy, transform.position.z);

       // if (temp > startposx + length) startposx += length; 
       //else if (temp < startposx - length) startposx -= length;
       //Debug.Log(length);


    }
}

