﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControls : MonoBehaviour
{
    // Start is called before the first frame update
    private Dictionary<string, KeyCode> buttoncontrol = new Dictionary<string, KeyCode>();
    public Text left, right, kuiva, jump, pause;
    private GameObject currentKey;

    private void Start()
    {
        buttoncontrol.Add("Left", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Left","A")));
        buttoncontrol.Add("Right", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Right","D")));
        buttoncontrol.Add("Jump", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Jump", "Space")));
        buttoncontrol.Add("Kuiva", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Kuiva", "Q")));
        buttoncontrol.Add("Pause", (KeyCode)System.Enum.Parse(typeof(KeyCode), PlayerPrefs.GetString("Pause", "Escape")));

        left.text = buttoncontrol["Left"].ToString();
        right.text =buttoncontrol["Right"].ToString();
        jump.text = buttoncontrol["Jump"].ToString();
        kuiva.text = buttoncontrol["Kuiva"].ToString();
        pause.text = buttoncontrol["Pause"].ToString();

    }

    private void OnGUI()
    {
        if (currentKey != null)
        {
            Event e = Event.current;
            if (e.isKey)
            {
                buttoncontrol[currentKey.name] = e.keyCode;
                Debug.Log(e.keyCode);
                currentKey.transform.GetChild(0).GetComponent<Text>().text = e.keyCode.ToString();
                currentKey = null;
            }
        }
    }

    public void ChangeKey(GameObject clicked)
    {
        currentKey = clicked;
    }

    public void SaveKeys()
    {
        foreach (var key in buttoncontrol) 
        {
            PlayerPrefs.SetString(key.Key, key.Value.ToString());
        }
        PlayerPrefs.Save();
    }


    public void PressStart()
    {
        SceneManager.LoadScene("Level1");
    }

    public void PressExit()
    {
        Application.Quit();
        Debug.Log("Exit");
    }
}
